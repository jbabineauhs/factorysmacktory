package com.jbabineau.factorysmacktory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;

// The benefit of this is that when Spring scans
// the package we can use those implementations
// and do not have to manage the beans.
@Component
public class AppContextFireModelFactory {

    @Autowired
    private ApplicationContext applicationContext;

    public FireModel GetFireModel(String stateCode, Date ratingDate) {

        // For Simplicity I am hard coding this but there should be a look up.
        FireModel result;

        // Pulls the model from the application context.
        if(stateCode.toUpperCase().equals("NH")) {
            result = (FireModel)applicationContext.getBean("AppNoLocationFireModel");
        } else if (stateCode.toUpperCase().equals("MA")) {
            result = (FireModel)applicationContext.getBean("AppNoScoreFireModel");
        } else if (stateCode.toUpperCase().equals("MD")) {
            result = (FireModel)applicationContext.getBean("AppNoScore3YrClaimFireModel");
        } else {
            result = (FireModel)applicationContext.getBean("AppNoRestrictionFireModel");
        }

        return result;
    }
}
