package com.jbabineau.factorysmacktory;

// This is an interface that exposes the one method the FireModel would have.
// This is dumbed down for this POC.
// Note: Both of the factories models use this interface.
public interface FireModel {
    String Run();
}
