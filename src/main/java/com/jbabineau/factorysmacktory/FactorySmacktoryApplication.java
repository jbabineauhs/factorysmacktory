package com.jbabineau.factorysmacktory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FactorySmacktoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(FactorySmacktoryApplication.class, args);
	}
}
