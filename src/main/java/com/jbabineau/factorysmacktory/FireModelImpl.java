package com.jbabineau.factorysmacktory;

import org.springframework.beans.factory.annotation.Autowired;

// This abstract class is only extended in the models used by the FireModelFactory
public abstract class FireModelImpl implements FireModel{
    // Make the models aware of the factory
    @Autowired
    private FireModelFactory fireModelFactory;

    // Registers the class with the factory.
    public FireModelImpl() {
        fireModelFactory.addInstance(this.getClass().getSimpleName(), this);
    }
}
