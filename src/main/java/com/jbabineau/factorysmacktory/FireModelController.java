package com.jbabineau.factorysmacktory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController()
@RequestMapping("/api")
public class FireModelController {

    @Autowired
    AppContextFireModelFactory appContextFireModelFactory;

    @Autowired
    FireModelFactory fireModelFactory;

    // This mapping calls the AppContextFireModel
    @RequestMapping("/app/{stateCode}")
    public String getFireModelApp(@PathVariable("stateCode")String stateCode) {
        return appContextFireModelFactory.GetFireModel(stateCode, new Date()).Run();
    }

    // This mapping calls the FireModelFactory
    @RequestMapping("/factory/{stateCode}")
    public String getFireModel(@PathVariable("stateCode") String stateCode) {
        return fireModelFactory.getFireModel(stateCode, new Date()).Run();
    }

}
