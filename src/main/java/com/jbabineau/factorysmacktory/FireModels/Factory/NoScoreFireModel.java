package com.jbabineau.factorysmacktory.FireModels.Factory;

import com.jbabineau.factorysmacktory.FireModelImpl;
import org.springframework.stereotype.Service;

@Service
public class NoScoreFireModel extends FireModelImpl{
    @Override
    public String Run() {
        return "Calling Run() inside " + this.getClass().getName();
    }
}
