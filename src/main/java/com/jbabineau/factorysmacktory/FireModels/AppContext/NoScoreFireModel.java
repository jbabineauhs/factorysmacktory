package com.jbabineau.factorysmacktory.FireModels.AppContext;

import com.jbabineau.factorysmacktory.FireModel;
import org.springframework.stereotype.Service;

// Instead of renaming this model to have a unique name
// I am cheating and using the @Service
// annotation to register the bean as a unique bean name.
@Service("AppNoScoreFireModel")
public class NoScoreFireModel implements FireModel {
    public String Run(){
        return "Calling Run() inside " + this.getClass().getName();
    }
}
