package com.jbabineau.factorysmacktory;

import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class FireModelFactory {
    private static Map<String, FireModel> fireModelMap = new HashMap<>();

    // Gets the model from the map based off name
    public static FireModel getFireModel(String stateCode, Date date){
        FireModel result;
        if(stateCode.toUpperCase().equals("NH")) {
            result = fireModelMap.get("NoLocationFireModel");
        } else if (stateCode.toUpperCase().equals("MA")) {
            result = fireModelMap.get("NoScoreFireModel");
        } else if (stateCode.toUpperCase().equals("MD")) {
            result = fireModelMap.get("NoScore3YrClaimFireModel");
        } else {
            result = fireModelMap.get("NoRestrictionFireModel");
        }
        return result;
    }

    // This method adds the model to the map
    public static void addInstance(String modelName, FireModel model) {
        fireModelMap.put(modelName, model);
    }
}
